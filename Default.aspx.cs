﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.ApplicationBlocks.Data;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Threading;
using System.Text.RegularExpressions;
using OpenPop.Pop3;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //    getList();
    }
    void getList()
    {
        DataSet ds = new DataSet();
        string UserName = ConfigurationManager.AppSettings["UserName"].ToString();
        string Password = ConfigurationManager.AppSettings["Password"].ToString();
        string Domain = ConfigurationManager.AppSettings["Domain"].ToString();
        int intMailCount = 1;
        string _mailSubjectCode = string.Empty;
        Pop3Client objClient = new Pop3Client();
        Int32 port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"].ToString());
        Boolean ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["SSL"].ToString());
        objClient.Connect(Domain, port, ssl);
        objClient.Authenticate(UserName, Password);
        Maildb objMaildb = new Maildb();
        //objMaildb.MessageId = i;
        //OpenPop.Mime.MessagePart plainTextPart = msg.FindFirstPlainTextVersion();
        int msgCount = objClient.GetMessageCount();
        for (int i = intMailCount; i <= msgCount; i++)
        {
            OpenPop.Mime.Message msg = objClient.GetMessage(i);
            _mailSubjectCode = Convert.ToString(msg.Headers.To[0]);
            //Maildb objMaildb = new Maildb();
            //objMaildb.MessageId = i;
            //OpenPop.Mime.MessagePart plainTextPart = msg.FindFirstPlainTextVersion();
        }
    }

    protected void btn_OnClick(object sender, EventArgs e)
    {

        //String _strResponse = SendEmail("sunny.kumar@kindlebit.com", "Test Email", "Hi, It is a test email");
        //Main();
        try
        {

            string email = "";
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");


            MailMessage msgMail = new MailMessage();
            String mailTo = "kindlebit.net@gmail.com";
            String mailCC = ConfigurationManager.AppSettings["mailToCC"].ToString();
            string mailFrom = "noreply@kindlebit.com";
            String mailSubject = "Test Mail Subject";
            bool mailIsHtml = true;
            String mailBody = "Test Mail";

            Match match = regex.Match(mailTo);
            if (match.Success)
            {
                Response.Write(mailTo + " is corrct");
            }
            else
            {
                Response.Write(mailTo + " is incorrct");
                return;
            }

            msgMail.To.Add(new MailAddress(mailTo));
            if (mailCC != "")
            {
                msgMail.CC.Add(new MailAddress(mailCC));
            }
            msgMail.Subject = mailSubject;
            msgMail.IsBodyHtml = mailIsHtml;
            msgMail.Body = mailBody;

            msgMail.From = new MailAddress(mailFrom);

            SmtpClient mailClient = new SmtpClient();
            string emailssl = string.Empty;
            mailClient.Host = "mail.kindlebit.com";
            mailClient.Credentials = new NetworkCredential("noreply@kindlebit.com", "Lux@123KB");
            emailssl = ConfigurationManager.AppSettings["emailssl"].ToString();
            if (emailssl.ToLower() == "true")
                mailClient.EnableSsl = true;
            else
                mailClient.EnableSsl = false;
            mailClient.Send(msgMail);

        }
        catch (Exception ex)
        {
            string err = ex.Message;
            Response.Write(ex.Message);
        }
    }
    void Main()
    {
        try
        {
            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("fromuniversalathletic@gmail.com", "nmfbwqmdlhmkbuqu"),
                EnableSsl = true
            };
            client.Send("fromuniversalathletic@gmail.com", "sunny.kumar@kindlebit.com", "test", "testbody");
        }
        catch (Exception ex)
        {

            throw;
        }
        //Console.WriteLine("Sent");
        //Console.ReadLine();
    }
    protected string SendEmail(string toAddress, string subject, string body)
    {
        string result = "Message Sent Successfully..!!";
        string senderID = "fromuniversalathletic@gmail.com";// use sender's email id here..
        const string senderPassword = "!@#32112"; // sender password here...
        try
        {
            SmtpClient smtp = new SmtpClient
            {
                Host = "smtp.gmail.com", // smtp server address here...
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new System.Net.NetworkCredential(senderID, senderPassword),
                Timeout = 30000,
                UseDefaultCredentials = true
            };
            MailMessage message = new MailMessage(senderID, toAddress, subject, body);
            smtp.Send(message);
        }
        catch (Exception ex)
        {
            result = "Error sending email.!!!";
        }
        return result;
    }
}

public class Maildb
{
    private int _MessageId;

    public int MessageId
    {
        get { return _MessageId; }
        set { _MessageId = value; }
    }

    private string _MsgDate;

    public string MsgDate
    {
        get { return _MsgDate; }
        set { _MsgDate = value; }
    }
    private string _MsgFrom;

    public string MsgFrom
    {
        get { return _MsgFrom; }
        set { _MsgFrom = value; }
    }
    private String _Subject;

    public String Subject
    {
        get { return _Subject; }
        set { _Subject = value; }
    }
    private String _Message;

    public String Message
    {
        get { return _Message; }
        set { _Message = value; }
    }
    public Maildb()
    {
        MessageId = 0;
        Message = "";
        Subject = "";
        MsgFrom = "";
        MsgDate = "";
    }

}
