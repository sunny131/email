﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class timer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Timer1.Enabled = true;
        Timer1.Interval = 86400000;
        Timer1.Tick += new EventHandler<EventArgs>(Timer1_Tick);
    }
    void Timer1_Tick(object sender, EventArgs e)
    {
        int i = (Convert.ToInt16(Label1.Text));
        i = i - 1;
        Label1.Text = i.ToString();
        if (i < 0)
        {
            Timer1.Enabled = false;
            Response.Redirect("TargetPage.aspx");
        }
    }
}
