﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsDal
/// </summary>
/// 
namespace DataAccessLayer.dbHelper
{
    public abstract class clsDAL
    {
        private static bool mIsExternalCon = false;

        static string CONFIG_DB_STRING = "ffrsconnectionstring";
        private static SqlConnection mCon = new SqlConnection(ConfigurationManager.ConnectionStrings[CONFIG_DB_STRING].ToString());
        //mCon = new SqlConnection();

        //public clsDAL()
        //{
        //  // string CONFIG_DB_STRING = "ConnectionString";

        //}

        //~clsDAL()
        //{
        //    if (mCon != null)
        //    {
        //        if (mCon.State != ConnectionState.Closed)
        //            mCon.Close();
        //        mCon = null;
        //    }
        //}
        public static SqlConnection GetConnection()
        {
            return mCon;
        }
        public static void SetConnection(SqlConnection pCon)
        {
            mIsExternalCon = true;
            mCon = pCon;
        }
        public static SqlCommand GetCommandObject(string pQry)
        {
            SqlCommand _cmd = new SqlCommand();
            _cmd.CommandText = pQry;
            return _cmd;
        }
        public static DataTable GetDataTable(string pQuery)
        {
            SqlCommand _cmd = new SqlCommand(pQuery);
            return GetDataTable(_cmd, CommandType.Text);
        }
        public static DataTable GetDataTable(SqlCommand pCmd)
        {
            return GetDataTable(pCmd, CommandType.Text);
        }
        public static DataTable GetDataTable(SqlCommand pCmd, CommandType pCmdType)
        {
            DataTable _dt = null;
            try
            {
                pCmd.Connection = mCon;
                pCmd.CommandType = pCmdType;
                SqlDataAdapter _adap = new SqlDataAdapter(pCmd);
                _dt = new DataTable();
                _adap.Fill(_dt);
                _adap = null;
            }
            catch (Exception ex)
            {
                if (mCon.State != ConnectionState.Closed)
                    mCon.Close();
                throw ex;
            }
            return _dt;
        }
        public static DataSet GetDataSet(string pQuery)
        {
            SqlCommand _cmd = new SqlCommand(pQuery);
            return GetDataSet(_cmd, CommandType.Text);
        }
        public static DataSet GetDataSet(SqlCommand pCmd)
        {
            return GetDataSet(pCmd, CommandType.Text);
        }
        public static DataSet GetDataSet(SqlCommand pCmd, CommandType pCmdType)
        {
            DataSet _ds = null;
            try
            {
                pCmd.Connection = mCon;
                pCmd.CommandType = pCmdType;
                SqlDataAdapter _adap = new SqlDataAdapter(pCmd);
                _ds = new DataSet();
                _adap.Fill(_ds);
                _adap = null;
            }
            catch (Exception ex)
            {
                if (mCon.State != ConnectionState.Closed)
                    mCon.Close();
                throw ex;
            }
            return _ds;
        }

        public static int InsertData(SqlCommand pCmd)
        {
            int _iID = -1;
            try
            {
                pCmd.Connection = mCon;
                pCmd.CommandType = CommandType.Text;
                pCmd.CommandText += ";Select SCOPE_IDENTITY() as id";
                if (!mIsExternalCon)
                    mCon.Open();
                _iID = Convert.ToInt32(pCmd.ExecuteScalar());
                if (!mIsExternalCon)
                    mCon.Close();
            }
            catch (Exception ex)
            {
                if (mCon.State != ConnectionState.Closed)
                    mCon.Close();
                throw ex;
            }
            return _iID;
        }

        public static int InsertData(SqlCommand pCmd, CommandType pCmdType)
        {
            int _iID = -1;
            try
            {
                pCmd.Connection = mCon;
                pCmd.CommandType = pCmdType;
                if (!mIsExternalCon)
                    mCon.Open();
                _iID = Convert.ToInt32(pCmd.ExecuteScalar());
                if (!mIsExternalCon)
                    mCon.Close();
            }
            catch (Exception ex)
            {
                if (mCon.State != ConnectionState.Closed)
                    mCon.Close();
                throw ex;
            }
            return _iID;
        }

        public static int ExecuteCommand(SqlCommand pCmd)
        {
            int _iRowsAffected = -1;
            try
            {
                pCmd.Connection = mCon;
                pCmd.CommandType = CommandType.Text;
                if (!mIsExternalCon)
                    mCon.Open();
                _iRowsAffected = Convert.ToInt32(pCmd.ExecuteNonQuery());
                if (!mIsExternalCon)
                    mCon.Close();
            }
            catch (Exception ex)
            {
                if (mCon.State != ConnectionState.Closed)
                    mCon.Close();
                throw ex;
            }
            return _iRowsAffected;
        }

        public static int ExecuteCommand(SqlCommand pCmd, CommandType pCmdType)
        {
            int _iRowsAffected = -1;
            try
            {
                pCmd.Connection = mCon;
                pCmd.CommandType = pCmdType;
                if (!mIsExternalCon)
                    mCon.Open();
                _iRowsAffected = Convert.ToInt32(pCmd.ExecuteNonQuery());
                if (!mIsExternalCon)
                    mCon.Close();
            }
            catch (Exception ex)
            {
                if (mCon.State != ConnectionState.Closed)
                    mCon.Close();
                throw ex;
            }
            return _iRowsAffected;
        }

        public static int ExecuteScalar(SqlCommand pCmd)
        {
            int _iResult = -888;
            try
            {
                pCmd.Connection = mCon;
                pCmd.CommandType = CommandType.Text;
                if (!mIsExternalCon)
                    mCon.Open();
                _iResult = Convert.ToInt32(pCmd.ExecuteScalar());
                if (!mIsExternalCon)
                    mCon.Close();
            }
            catch (SqlException ex)
            {
                return -888;
            }
            catch (Exception ex)
            {
                return -88;
            }
            finally
            {
                if (mCon.State != ConnectionState.Closed)
                    mCon.Close();

            }
            return _iResult;
        }

        public static int ExecuteScalar(SqlCommand pCmd, CommandType pCmdType)
        {
            int _iResult = -888;
            try
            {
                pCmd.Connection = mCon;
                pCmd.CommandType = pCmdType;
                if (!mIsExternalCon)
                    mCon.Open();
                _iResult = Convert.ToInt32(pCmd.ExecuteScalar());
                if (!mIsExternalCon)
                    mCon.Close();
            }
            catch (Exception ex)
            {
                if (mCon.State != ConnectionState.Closed)
                    mCon.Close();
                throw ex;
            }
            return _iResult;
        }
    }
}