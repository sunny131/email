﻿using System;
//using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using RestApiClient;
using System.Data.SqlClient;
using System.Data;
using DataAccessLayer.dbHelper;
using System.Data.Odbc;
using System.Configuration;

public partial class Alias : System.Web.UI.Page
{
    private string fileNevCSV;
    private string dirCSV;
    private string strFormat;
    private string strEncoding;
    private string FTP = ConfigurationManager.AppSettings["FTP"].ToString();
    private string FTPUID = ConfigurationManager.AppSettings["FTPUID"].ToString();
    private string FTPPWD = ConfigurationManager.AppSettings["FTPPWD"].ToString();
    private string FileName = ConfigurationManager.AppSettings["FTPFileName"].ToString();
    DataTable dtGoogle = new DataTable();
    public string FileNevCSV
    {
        get { return fileNevCSV; }
        set { fileNevCSV = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        dtGoogle = CerateDataTableFromFTP();
        //GetEmail();
        //b _obj = new b();
        //DataTable _dt = _obj.getdatatable();
        ExportToExcel1(dtGoogle, "AAC");
    }
    public class b : clsDAL
    {
        public DataTable getdatatable()
        {
            SqlCommand _objCmd = new SqlCommand();
            DataTable _dtInfo = new DataTable();
            _objCmd.CommandText = "GetSMSGatewayDetails";
            //_objCmd.CommandType = CommandType.StoredProcedure;
            _dtInfo = b.GetDataTable(_objCmd, CommandType.StoredProcedure);
            return _dtInfo;
        }
    }
    protected void ExportToExcel(DataTable dataTable, string fileName)
    {
        HttpContext context = HttpContext.Current;
        context.Response.Clear();
        foreach (DataColumn column in dataTable.Columns)
        {
            context.Response.Write(column.ColumnName + ",");
        }
        context.Response.Write(Environment.NewLine);

        foreach (DataRow row in dataTable.Rows)
        {
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                context.Response.Write(row[i].ToString() + ",");
            }
            context.Response.Write(Environment.NewLine);
        }
        context.Response.ContentType = "text/xls";
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");
        context.Response.End();
    }

    public DataTable CerateDataTableFromFTP()
    {
        DataTable dsTable = new DataTable();
        try
        {
            DownloadCSV();
            if (File.Exists(@"d:\TestQuery.csv"))
            {
                this.FileNevCSV = "TestQuery.csv";
                this.dirCSV = @"d:\";
                dsTable = GetDataCSVFIle(this.dirCSV + this.fileNevCSV);
                //MessageLog("Automatic Account Creation", "DataRows Found in CSV file :" + dsTable.Rows.Count);
            }
            else
            {
                //MessageLog("Automatic Account Creation", "CSV File Not Found.");
            }
        }
        catch (Exception ex)
        {
            //MessageLog("Automatic Account Creation", "ERROR : - " + ex.Message);
        }
        return dsTable;
    }


    private static DataTable GetDataCSVFIle(string filePath)
    {
        DataTable dtAutomatic = new DataTable();

        FileStream aFile = new FileStream(filePath, FileMode.OpenOrCreate);
        try
        {
            string strLine;
            string[] strArray;
            char[] charArray = new char[] { ',' };
            DataSet ds = new DataSet();
            DataTable dt = ds.Tables.Add("TheData");
            StreamReader sr = new StreamReader(aFile);
            try
            {
                strLine = sr.ReadLine();

                strArray = strLine.Split(charArray);
                for (int x = 0; x <= strArray.GetUpperBound(0); x++)
                {
                    dt.Columns.Add(strArray[x].Replace("\"", "").Trim());
                }
                strLine = sr.ReadLine();
                while (strLine != null)
                {
                    strArray = strLine.Split(charArray);
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i <= strArray.GetUpperBound(0); i++)
                    {
                        if (i == 12) // Last column should be IP address
                        {
                            break;
                        }
                        dr[i] = strArray[i].Replace("\"", "").Trim();
                    }
                    dt.Rows.Add(dr);
                    strLine = sr.ReadLine();
                }
                sr.Close();
                if (ds.Tables.Count > 0)
                    dtAutomatic = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //MessageLog("Automatic Account Creation", ex.ToString());
                //MessageLog("Automatic Account Creation", "Read datatable from testQuery.csv");
                sr.Close();
                sr.Dispose();
            }
        }
        catch (Exception ex)
        {

        }
        return dtAutomatic;
    }


    public DataTable LoadCSV(string filePath)
    {
        strFormat = "Delimited(,)";
        strEncoding = "ANSI";
        writeSchema();
        DataSet ds = new DataSet();
        try
        {
            // Creates and opens an ODBC connection
            string strConnString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + this.dirCSV.Trim() + ";Extensions=asc,csv,tab,txt;Persist Security Info=False";

            string sql_select;
            OdbcConnection conn;
            conn = new OdbcConnection(strConnString.Trim());
            conn.Open();
            sql_select = "select * from [" + this.FileNevCSV.Trim() + "]";
            OdbcDataAdapter obj_oledb_da = new OdbcDataAdapter(sql_select, conn);
            obj_oledb_da.Fill(ds, "csv");
            conn.Close();

            //MessageLog("Spread sheetgear called", "");
            //SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook(filePath);
            //workbook.WorkbookSet.GetLock();
            //ds  = workbook.GetDataSet(SpreadsheetGear.Data.GetDataFlags.FormattedText);

            //MessageLog("Spread sheetgear called", "Successfully converted to Dataset");
            //workbook.Close(); 
            //workbook = null;

        }
        catch (Exception ex)
        {
            //MessageLog("Automatic Account Creation", "ERROR Loading CSV To DataSet." + ex.Message);
        }

        DataTable dtAutomatic = new DataTable();
        if (ds.Tables.Count > 0)
            dtAutomatic = ds.Tables[0];

        if (File.Exists(@"d:\TestQuery.csv"))
        {
            File.Delete(@"d:\TestQuery.csv");
            //MessageLog("Automatic Account Creation", "CSV file Deleted Successfully.");
        }
        //MessageLog("Automatic Account Creation", "CSV file Converted to DataTable");
        return dtAutomatic;
    }
    private void writeSchema()
    {
        try
        {
            FileStream fsOutput = new FileStream(this.dirCSV + "\\schema.ini", FileMode.Create, FileAccess.Write);
            StreamWriter srOutput = new StreamWriter(fsOutput);
            string s1, s2, s3, s4, s5;
            s1 = "[" + this.FileNevCSV + "]";
            //s2 = "ColNameHeader=" + chkFirstRowColumnNames.Checked.ToString();
            s2 = "ColNameHeader=" + "true";
            s3 = "Format=" + this.strFormat;
            s4 = "MaxScanRows=25";
            s5 = "CharacterSet=" + this.strEncoding;
            srOutput.WriteLine(s1.ToString() + "\r\n" + s2.ToString() + "\r\n" + s3.ToString() + "\r\n" + s4.ToString() + "\r\n" + s5.ToString());
            srOutput.Close();
            fsOutput.Close();
        }
        catch (Exception ex)
        {
        }
        finally
        { }
    }

    public void DownloadCSV()
    {
        FileStream outputStream = new FileStream(@"d:\TestQuery.csv", FileMode.OpenOrCreate);
        try
        {
            WebClient client = new WebClient();
            client.Credentials = new NetworkCredential(FTPUID, FTPPWD);
            byte[] bytedata = client.DownloadData(new Uri(FTP + FileName));
            outputStream.Write(bytedata, 0, bytedata.Length);
            //MessageLog("Automatic Account Creation", "CSV Downloaded to testQuery.csv");
        }
        catch (Exception ex)
        {
            //MessageLog("MAIN DownloadCSV ERROR", ex.Message);
        }
        finally
        {
            outputStream.Close();
            outputStream.Dispose();
            GC.Collect();
            //outputStream.Flush();
        }
    }

    private void SaveAsCSV(string RespnseData)
    {

        //string CSVFileName = Server.MapPath("Data") + @"\data.csv";
        #region to create .CSV file

        FileStream fs = new FileStream(@"d:\TestQuery.csv", FileMode.OpenOrCreate, FileAccess.Write);
        fs.Flush();
        fs.Close();
        #endregion

        string CSVFileName = fs.Name.ToString();
        using (StreamWriter writer = new StreamWriter(CSVFileName))
        {
            writer.Write(RespnseData);
            //MessageLog("Automatic Account Creation", "Data Write to csv file successfuly.");
        }

        // coding test block start
        // Create a workbook from an Excel file
        //String ssFile = Server.MapPath("files/spiceorder.xls");

        //String ssFile = fs.Name.ToString();
        //SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook(ssFile);
        //workbook.WorkbookSet.GetLock();

        //DataSet dataSet = workbook.GetDataSet(SpreadsheetGear.Data.GetDataFlags.FormattedText);           

        //// Get a range from an existing defined name.
        //SpreadsheetGear.IRange range = workbook.Names["TestQuery.csv"].RefersToRange;

        //// Hide rows which do not have units > 100.
        //int unitsCol = 2;
        //foreach (SpreadsheetGear.IRange units in range[1, unitsCol, range.RowCount - 1, unitsCol])
        //{
        //    object val = units.Value;
        //    if (!((val is double) && (double)val > 100.0))
        //        units.Rows.Hidden = true;
        //}

        // Get a DataTable from the range ignoring the hidden rows.
        //DataTable dataTable = GetDataTable(range, SpreadsheetGear.Data.GetDataFlags.FormattedText);


        // Bind a DataGrid to the DataTable
        // DataGrid1.DataSource = dataTable;
        //DataGrid1.DataBind();

    }

    protected void ExportToExcel1(DataTable dataTable, string fileName)
    {
        if (dataTable.Rows.Count > 0)
        {
            string filename = "DownloadMobileNoExcel.xls";
            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
            DataGrid dgGrid = new DataGrid();
            dgGrid.DataSource = dataTable;
            dgGrid.DataBind();

            //Get the HTML for the control.
            dgGrid.RenderControl(hw);
            //Write the HTML back to the browser.
            //Response.ContentType = application/vnd.ms-excel;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
            this.EnableViewState = false;
            Response.Write(tw.ToString());
            Response.End();
        }
    }
    void GetEmail()
    {
        HttpWebRequest myHttpWebRequest = null;     //Declare an HTTP-specific implementation of the WebRequest class.
        HttpWebResponse myHttpWebResponse = null;   //Declare an HTTP-specific implementation of the WebResponse class
        XmlDocument myXMLDocument = null;           //Declare XMLResponse document
        XmlTextReader myXMLReader = null;           //Declare XMLReader

        try
        {
            //Create Request
            //RestApiClient.RestApiClient _objApiClient = new RestApiClient.RestApiClient("https://identity.api.rackspacecloud.com/v1.0/", "cI9vgKvaly9fEnGeQL9G", "4tLt7tC2RtDlDjLVNcbIwT9+2qkks1hL/gT9BarU");
            //_objApiClient.Get("1115835/domains/enotifyffrs.com/rs/aliases", "text/xml");

            //customers/123456789/domains/example.com/rs/mailboxes/john.smith/alias

            myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create("https://api.smtp.com/v1/account.json?api_key=6380f2ba4d2afc825d78743e35f008c92fa50580");
            //https://api.emailsrvr.com/v0/customers/(customer account number)/domains/(domain name)/rs/mailboxes/(mailbox name)/alias/(alias name)
            myHttpWebRequest.Method = "GET";
            myHttpWebRequest.Credentials = new NetworkCredential("michael.koechler@swissphone.com", "=+Wvux/uf");

            myHttpWebRequest.ContentType = "application/json; encoding='utf-8'";

            //Get Response
            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            //Now load the XML Document
            myXMLDocument = new XmlDocument();

            //Load response stream into XMLReader
            myXMLReader = new XmlTextReader(myHttpWebResponse.GetResponseStream());
            myXMLDocument.Load(myXMLReader);
        }
        catch (WebException ex)
        {
            if (ex.Status == WebExceptionStatus.ProtocolError)
            {
                if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    int code = (Int32)myHttpWebResponse.StatusCode;
                    {
                        // handle the 404 here
                    }
                }
            }
        }
        finally
        {
            myHttpWebRequest = null;
            myHttpWebResponse = null;
            myXMLReader = null;
        }
    }
    void AddAliasEmail()
    {
        HttpWebRequest myHttpWebRequest = null;     //Declare an HTTP-specific implementation of the WebRequest class.
        HttpWebResponse myHttpWebResponse = null;   //Declare an HTTP-specific implementation of the WebResponse class
        XmlDocument myXMLDocument = null;           //Declare XMLResponse document
        XmlTextReader myXMLReader = null;           //Declare XMLReader

        try
        {
            //Create Request
            myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create("http://api.emailsrvr.com/v0/customers/1115835/domains/enotifyffrs.com/rs/aliases/alias1',aliasEmails'=>'enotify@enotifyffrs.com,sunny@enotifyffrs.com'},Formats.xml");
            //            customers/1115835/domains/resellerallservices.net/rs/aliases/alias1',
            //{'aliasEmails'=>'enotify@enotifyffrs.com,sunny@enotifyffrs.com'
            //}
            myHttpWebRequest.Method = "POST";
            myHttpWebRequest.ContentType = "text/xml; encoding='utf-8'";

            //Get Response
            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            //Now load the XML Document
            myXMLDocument = new XmlDocument();

            //Load response stream into XMLReader
            myXMLReader = new XmlTextReader(myHttpWebResponse.GetResponseStream());
            myXMLDocument.Load(myXMLReader);
        }
        catch (Exception myException)
        {
            throw new Exception("Error Occurred in AuditAdapter.getXMLDocumentFromXMLTemplate()", myException);
        }
        finally
        {
            myHttpWebRequest = null;
            myHttpWebResponse = null;
            myXMLReader = null;
        }
    }
}
